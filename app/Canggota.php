<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Canggota extends Model
{
    protected $fillable = [
        'nama',
        'img'
    ];
}
