<?php

namespace App\Http\Controllers\back_end\pendaftaran;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Canggota;
use App\Test;
use DB;

class pendaftaranController extends Controller
{
    public function index () {
     	$data = Canggota::select('nama', 'kelas', 'semester', 'email', 'no_hp', 'id')
        ->get();
        
        
    	return view ('back_end.pendaftaran_anggota.tampil_page', compact('data')); 
    }

    public function viewDetail(Request $request, $id){
      $ids = $id;
      $data = Canggota::select('nama', 'kelas', 'semester', 'email', 'no_hp')
      	->where('id', '=', $ids)
        ->get();
    	return view ('back_end.pendaftaran_anggota.view_detail', compact('data')); 
    }

    public function viewInsert(){
    	return view ('back_end.pendaftaran_anggota.insert', compact('data')); 
    }

    public function upload(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|nullable|max:100',
            'file' => 'required|file|max:2000'
        ]);

        $uploadedFile = $request->file('file');        
        $path = $uploadedFile->store('public/images');
        $file = Test::create([
            'nama' => $request->title ?? $uploadedFile->getClientOriginalName(),
            'img' => $path
        ]);
        return redirect()
            ->back()
            ->withSuccess(sprintf('File %s has been uploaded.', $file->nama));  
            
    }


}
