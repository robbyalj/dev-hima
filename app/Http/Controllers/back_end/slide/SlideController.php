<?php

namespace App\Http\Controllers\back_end\slide;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Slide;
use DB;

class SlideController extends Controller
{
    public function index()
    {
        $data = Slide::select('id','judul', 'subjudul', 'deskripsi', 'img')
        ->get();
        $no = 1;
        return view ('back_end.slide.index', compact('data')); 
    }

    
    public function create()
    {
        return view('back_end.slide.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $save = Home::create('image_name','image')
        ->post();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
