<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Test extends Model
{
     protected $fillable = [
        'nama',
        'img'
    ];
}
