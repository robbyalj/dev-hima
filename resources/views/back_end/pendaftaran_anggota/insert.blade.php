@extends('back_end.layouts.master')

@section('content')
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Insert Calon Anggota
      <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="/kasir"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="/kasir"></i> calon anggota</a></li>
      <li class="active"> insert calon anggota</li>
    </ol>
  </section>
<!-- Main content -->
  <section class="content">
    <div class="box">
            <div class="box-header">
              <h3 class="box-title">Insert Calon Anggota</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
                    @if(session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                <form action="{{ route('file.upload') }}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                {{method_field('POST')}}
                <div class="box-body">
                  <div class="row">                        
                      <div class="col-lg-12 col-sm-12">
                          <div class="form-group">
                              <label>Nama</label>
                              <input type="hidden" value="{{csrf_token()}}" name="_token" />
                              <input type="text" name="jenis" class="form-control" >
                              <label>Email</label>
                              <input type="text" name="jenis" class="form-control" >
                              <label>No Hp</label>
                              <input type="text" name="jenis" class="form-control" >
                              <label>Tahun Masuk</label>
                              <input type="text" name="jenis" class="form-control" >
                              <label>Semester</label>
                              <input type="text" name="jenis" class="form-control" >
                              <label>Kelas</label>
                              <input type="text" name="jenis" class="form-control" >
                              <label>Hobby</label>
                              <input type="text" name="jenis" class="form-control" >
                              <label>Instagram</label>
                              <input type="text" name="jenis" class="form-control" >
                              <label>Facebook</label>
                              <input type="text" name="jenis" class="form-control" >
                              <label>Alasan Bergabung</label>
                              <input type="text" name="title" class="form-control" >
                              <label>Photo</label>
                              <input id="input-b2" name="input-b2" type="file" class="file" data-show-preview="false">
                              <label>File</label>
                              <input type="file" name="file">
                              <span class="help-block text-danger">{{ $errors->first('file') }}</span>
                          </div>
                          <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                      </div>
                  </div> <!-- row -->    
                </div><!--body-->
                </form>
            </div>
            <!-- /.box-body -->
          </div>
  </section>
<!-- /.content -->

@endsection
