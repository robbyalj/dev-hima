@extends('back_end.layouts.master')

@section('content')
<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Detail Calon Anggota
      <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="/kasir"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="/kasir"></i> calon anggota</a></li>
      <li class="active"> detail calon anggota</li>
    </ol>
  </section>
<!-- Main content -->
  <section class="content">
    <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Calon Anggota</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>nama</th>
                  <th>Kelas</th>
                  <th>Semester</th>
                  <th>Email</th>
                  <th>No tlp</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                @foreach($data as $p)
                <tr>
                  <td>1</td>
                  <td>{{$p->nama}}</td>
                  <td>{{$p->kelas}}</td>
                  <td>{{$p->semester}}</td>
                  <td>{{$p->email}}</td>
                  <td>{{$p->no_hp}}</td>
                  <td><span class="label label-success">Approved</span></td>
                  <td>
                    <a href="#" class="btn btn-warning"><span class="glyphicon glyphicon-pencil"></span></a>
                    <a href="#" class="btn btn-danger"><span class="glyphicon glyphicon-glyphicon glyphicon-remove"></span></a>
                  </td>   
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
  </section>
<!-- /.content -->

@endsection
