@extends('admin.adminlte')

@section('content')

<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Data Image Home Header
      <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="/admin"><i class="fa fa-dashboard"></i>Home</a></li>
      <li><a href="/admin/home/"><i></i>Image Home Header</a></li>
      <li class="active">Add Data</li>
    </ol>
  </section>
  <br><br>
  	<section>
  		<form>
			<div class="form-group">
                  <label>Nama Gambar</label>
                  <textarea class="form-control" rows="3" placeholder="Masukan Nama Gambar"></textarea>
                </div>

		<div  class="box-footer">
                  <label for="exampleInputFile">Masukan Gambar</label>
                  <input type="file" id="InputFile">
                  <p class="help-block">Pastikan Terisi Degan Benar.</p>
                </div><br>
	<!-- /.box-body -->

              <form action="{{route('home.store')}}" method="post">
            	<button  class="btn btn-primary">
            		simpan
            	</button>
            	
            </form>
</section>

@endsection