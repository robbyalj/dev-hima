@extends('back_end.layouts.master')

@section('content')

<!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Slide Image
      <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="/admin"><i class="fa fa-dashboard"></i>Home</a></li>
      <li class="active">Image Slide Header</li>
    </ol>
  </section>
<!-- Main content -->
  <section class="content">
    <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Image Slide</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Judul</th>
                  <th>Subjudul</th>
                  <th>Image</th>
                  <th>status</th>
                  <th>Action</th>
                </tr>
                @php ($no = 1)
                @foreach($data as $p)
                <tr>
                  <td>{{$no}}</td>
                  <td>{{$p->judul}}</td>
                  <td>{{$p->subjudul}}</td>
                  <td>{{$p->img}}</td>
                  <td><span class="label label-success">Approved</span></td>
                  <td>
                    <a href="" class="btn btn-primary"><span class="glyphicon glyphicon-zoom-in"></span></a>
                     <a href="{{action('back_end\pendaftaran\pendaftaranController@viewDetail',$p->id)}}" class="btn btn-warning"><span class="glyphicon glyphicon-pencil"></span></a>
                    <a href="#" class="btn btn-danger"><span class="glyphicon glyphicon-glyphicon glyphicon-remove"></span></a>
                  </td>   
                </tr>
                @endforeach
              </table>
            </div>
            <!-- /.box-body -->
          </div>
  </section>
<!-- /.content -->

@endsection