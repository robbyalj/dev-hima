<!DOCTYPE html>
<html lang="en">
  <head> 
      @extends('front_end.layout.master')
  </head>

  <body>
  @include('front_end.layout.menu')
   
    <div id="carouselExampleIndicators" class="carousel height-slide" data-ride="carousel" style="background-color: #256E95;">
          <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          </ol>
          <div class="carousel-inner height-slide" role="listbox">
            <div class="carousel-item active">
              <img class="d-block img-fluid" src="img/bcg.png" alt="First slide">
            </div>
            <div class="carousel-item">
              <img class="d-block img-fluid" src="img/banner.png" alt="Second slide">
              <div class="text-banner2 ">
                <h3 class="banner-txt banner-mini-size">APA YANG TERJADI</h3>
                <h3 class="banner-txt banner-mini-size">PADA TEKNOLOGI HARI INI?</h3>
                <br>
                <center><button type="button" class="btn btn-warning btn-warning-text" href="tdtechno">Temukan Disini</button></center>
              </div>
            </div>
          </div>
              <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                 <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                 <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
               </a>
        </div>
        <section class="content-section-uu ">
          <!-- PROFILE -->
          <section class="content-section-a">
            <div class="container padding-cont">
              <div class="row">
                <div class="col-md-6 ml-auto"> 
                  <h2 class="section-heading1"><u>Profile</u></h2>
                  <p class="lead">
                       <font color="white"> HIMATIF merupakan Himpunan Mahasiswa Teknik Informatika di STT Bandung </font>
                  </p>
                  <center><a href="underconstruct" class="btn btn-transparan btn-lg">Read More</a></center>
                </div>
                <div class="col-md-6 ml-auto">
                  <img class="img-fluid" src="img/gambaratas.png" alt="">
                </div>
              </div>
            </div>
          </section>
          <!-- /.EVENT -->
          <!-- mmmm -->
          <section class="content-section-b">
            <div class="container padding-cont">
              <h2 class="section-heading2"><u>Our Nearest Event</u></h2>
            <center>
            <div class="row">
                <div class="col-sm-6 col-md-4">
                  <div class="img-thumbnail">
                    <a href="underconstruct"><img class="img-fluid" src="img/fest3.jpg" alt=""></a>
                    <div class="caption">
                        <h5 class="margin-top">"Pekan Matika"</h5>
                        <p>...</p>
                        <p><a href="underconstruct" class="btn btn-primary" role="button">More</a> <a href="underconstruct" class="btn btn-default" role="button">Share</a></p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="img-thumbnail">
                      <a href="underconstruct"><img class="img-fluid" src="img/fest1.jpg" alt=""></a>
                      <div class="caption">
                          <h5 class="margin-top">"Food Festival"</h5>
                          <p>...</p>
                          <p><a href="underconstruct" class="btn btn-primary" role="button">More</a> <a href="underconstruct" class="btn btn-default" role="button">Share</a></p>
                      </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4">
                    <div class="img-thumbnail">
                      <a href="underconstruct"><img class="img-fluid" src="img/fest2.jpg" alt=""></a>
                      <div class="caption">
                          <h5 class="margin-top">Indie Open Air</h5>
                          <p>...</p>
                          <p><a href="underconstruct" class="btn btn-primary" role="button">More</a> <a href="underconstruct" class="btn btn-default" role="button">Share</a></p>
                      </div>
                    </div>
                </div>
            </div><br>
            
            <a href="underconstruct" class="btn btn-trans-red btn-lg">Read More</a>
            </center>
            </div>
          </section>
          <!-- /.IFSHOP -->
          <section class="content-section-c">
              <div class="container padding-cont">
                <center>
                <h2 class="section-heading2"><u><p align= "center">IFShop</p></u></h2>
                <div class="row">
                  <div class="col-sm-6 col-md-4">
                    <div class="img-thumbnail">
                      <img class="img-fluid" src="img/kaos1.png" alt="">
                      <div class="caption">
                          <h5 class="margin-top">Kaos Programmer</h5>
                          <p>IDR 100.000,-</p>
                          <p><a href="underconstruct" class="btn btn-primary" role="button">Buy</a></p>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-4">
                    <div class="img-thumbnail">
                      <img class="img-fluid" src="img/kaos1.png" alt="">
                      <div class="caption">
                          <h5 class="margin-top">Kaos Programmer</h5>
                          <p>IDR 100.000,-</p>
                          <p><a href="underconstruct" class="btn btn-primary" role="button">Buy</a></p>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-4">
                    <div class="img-thumbnail">
                      <img class="img-fluid" src="img/kaos1.png" alt="">
                      <div class="caption">
                          <h5 class="margin-top">Kaos Programmer</h5>
                          <p>IDR 100.000,-</p>
                          <p><a href="underconstruct" class="btn btn-primary" role="button">Buy</a></p>
                      </div>
                    </div>
                  </div>
                </div><br>
                 <a href="underconstruct" class="btn btn-trans-red btn-lg">Read More</a>
                 </center>
              </div>
          </section>
          <!--todays tekno-->
          <section class="content-section-d">
            <div class="container padding-cont">
              <center><h2 class="section-heading2"><u><p style="text-align:center">Today's Techno</p></u></h2></center>
              <div class="row">
                <div class="col-md-4 ml-auto">
                  <img class="img-fluid" src="img/dekstop.png" alt="">
                    <h5 class="section-heading">Apa yang terjadi pada dunia hari ini</h5>
                      <p class="font-size">Turn your 2D designs into high quality, 3D product shots in seconds using free Photoshop actions by
                      <a target="_blank" href="underconstruct">PSDCovers</a>! Visit their website to download some of their awesome, free photoshop actions!</p>
                </div>
                <div class="col-md-4 ml-auto">
                  <img class="img-fluid" src="img/dekstop2.png" alt="">
                    <h5 class="section-heading">Apa yang terjadi pada dunia hari ini</h5>
                      <p class="font-size">Turn your 2D designs into high quality, 3D product shots in seconds using free Photoshop actions by
                      <a target="_blank" href="underconstruct">PSDCovers</a>! Visit their website to download some of their awesome, free photoshop actions!</p>
                </div>
                <div class="col-md-4 ml-auto">
                  <img class="img-fluid" src="img/dekstop.png" alt=""><br>
                    <h5 class="section-heading">Apa yang terjadi pada dunia hari ini</h5>
                      <p class="font-size">Turn your 2D designs into high quality, 3D product shots in seconds using free Photoshop actions by
                      <a target="_blank" href="underconstruct">PSDCovers</a>! Visit their website to download some of their awesome, free photoshop actions!</p>
                </div>
              </div>
            </div>
            <!-- /.container -->
            <br>
            <center>
            <a href="underconstruct" class="btn btn-trans-red btn-lg">Read More</a>
            </center>
          </section>
        </section>
    @include('front_end.layout.footer')
  </body>