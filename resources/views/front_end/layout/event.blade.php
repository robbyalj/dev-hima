<!DOCTYPE html>
<html lang="en">
  <head>
     @extends('front_end.layout.master')
  </head>
  <body> 
@include('front_end.layout.menu')

    <div class="ml-auto">
        <img class="img-fluid" src="img/event.png" alt="">
    </div><br><br>
    
           <div class="col-md-4">
            <h2 class="section-heading9">Upcoming Events</h2>
           </div>
           <hr width="90%" align="center">
        </section>

    <!-- Upcoming Events -->
<section class="content-section-g">
    
    <section class="content-section-f">
      <div class="container padding-cont2">
        <div class="row">
              <div class="col-md-13"> 
              <h2 class="section-heading4">Juni</h2>
              <h2 class="section-heading4">23</h2>
              </div>

             <div class="col-md-4 ml-auto">
               <div class="img-thumbnail">
                  <img class="img-fluid" src="img/contohevent.jpg">
               </div> 
             </div>

                  <div class="col-md-6"> 
                        <h2 class="section-heading3">Title of The Risen Event</h2>
                          <p class="lead">
                          <font color="black"> 10.15 California Ave Los Angeles CA</font><br>
                          <font color="black"> 7.00 pm until 8.00 pm</font><br>
                          <br>
                            <font color="black">Turn your 2D designs into high quality, 3D product shots in seconds free Photoshop actions by PSD Covers! Visit their website to download some of their awesome, free photoshop actions! </font>
                          </p>
                          <a href="#"><p class="lead"> View Event Details >> </p></a>
                  </div>  
            </div>
          </div>   
    </section>
    <hr width="90%" align="center">

            <section class="content-section-f">
      <div class="container padding-cont2">
        <div class="row">
              <div class="col-md-13"> 
              <h2 class="section-heading4">April</h2>
              <h2 class="section-heading4">23</h2>
              </div>

             <div class="col-md-4 ml-auto">
               <div class="img-thumbnail">
                  <img class="img-fluid" src="img/contohevent2.jpg">
               </div> 
             </div>

                  <div class="col-md-6"> 
                        <h2 class="section-heading3">Title of The Risen Event</h2>
                          <p class="lead">
                          <font color="black"> 10.15 California Ave Los Angeles CA</font><br>
                          <font color="black"> 7.00 pm until 8.00 pm</font><br>
                          <br>
                            <font color="black">Turn your 2D designs into high quality, 3D product shots in seconds free Photoshop actions by PSD Covers! Visit their website to download some of their awesome, free photoshop actions! </font>
                          </p>
                          <a href="#"><p class="lead"> View Event Details >> </p></a>
                  </div>  
            </div>
          </div>   
    </section>
    <hr width="90%" align="center">

    <section class="content-section-f">
      <div class="container padding-cont2">
        <div class="row">
              <div class="col-md-13"> 
              <h2 class="section-heading4">Dec</h2>
              <h2 class="section-heading4">23</h2>
              </div>

             <div class="col-md-4 ml-auto">
               <div class="img-thumbnail">
                  <img class="img-fluid" src="img/contohevent3.jpg">
               </div> 
             </div>

                  <div class="col-md-6"> 
                        <h2 class="section-heading3">Title of The Risen Event</h2>
                          <p class="lead">
                          <font color="black"> 10.15 California Ave Los Angeles CA</font><br>
                          <font color="black"> 7.00 pm until 8.00 pm</font><br>
                          <br>
                            <font color="black">Turn your 2D designs into high quality, 3D product shots in seconds free Photoshop actions by PSD Covers! Visit their website to download some of their awesome, free photoshop actions! </font>
                          </p>
                          <a href="#"><p class="lead"> View Event Details >> </p></a>
                  </div>  
            </div>
          </div>   
    </section>
    <hr width="90%" align="center">

</section>
@include('front_end.layout.footer')
 </body>
</html>