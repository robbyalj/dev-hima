<!--Footer-->
<!-- nnnn -->
   <footer>
      <div class="container">
        <div class="row animated opacity mar-bot20" data-andown="fadeIn" data-animation="animation">
          <div class="col-md-4">
           <h5 class="posit">Our Location</h5>
           <p class="posco small">
              Jl. Soekarno Hatta No. 378 Bandung Jawa Barat 
           </p>
          </div>
          <div class="col-md-4" style="padding-bottom: 20px;">
            <ul class="social-network social-circle">
              <li><a href="underconstruct" class="icoYoutube" title="Youtube"><i class="fa fa-youtube"></i></a></li>
              <li><a href="underconstruct" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
              <li><a href="underconstruct" class="icoTwitter" title="Twitter"><i class="fa fa-twitter"></i></a></li>
              <li><a href="underconstruct" class="icoGoogle" title="Google +"><i class="fa fa-google-plus"></i></a></li>
              <li><a href="underconstruct" class="icoInsta" title="Instagram"><i class="fa fa-instagram"></i></a></li>
            </ul>
          </div>
          <div class="col-md-4">
              <h5 class="posit">About HIMATIF STTBandung</h5>
              <p class="posco small">HIMATIF merupakan himpunan mahasiswa Informatika di STTBandung.</p>
          </div>
          <div class="faq">
           <ul class="list-inline">
             <li class="list-inline-item"><a href="underconstruct" class="color">Home</a></li>
             <li class="list-inline-item color">|</li>
             <li class="list-inline-item"><a href="underconstruct" class="color">FAQ</a></li>
             <li class="list-inline-item color">|</li>
             <li class="list-inline-item"><a href="underconstruct" class="color">HIMATIF STTBandung Main Website</a></li>
           </ul>
          </div>
          <div class="faq">
          <p class="copyright">&copy;2017 HIMATIF STTBandung. All Rights Reserved</p>
          </div>
        </div>
      </div>   
   </footer> 
   <a href="#" class="scrollup"><i class="fa fa-chevron-up"></i></a>