<!DOCTYPE html>
<html lang="en">
  <head>
     @extends('front_end.layout.master')
  </head>
  <body> 
  	@include('front_end.layout.menunologin')

    <div class="container">
		<center><img class="img-fluid" src="img/1.png" style="width: 60px; margin-top: 50px;"></center>
	</div>
	<div class="container login-panel">
			<center><p class="login">Reset your Password Here</p></center>
			<div class="login-form">
				<div class="form-group">
				  <label for="usr">Insert your Email:</label>
				  <input type="text" class="form-control" id="usr" placeholder="Email.."] >
				</div>
				<button type="button" class="btn btn-success btn-block">Reset Password</button>
        <div class="row">
          <div class="col-sm-6">
            <p class="frgt-pass"><a href="forgot.php">Already have account? Sign In Here</a></p>
          </div>
          <div class="col-sm-6">
            <p class="regist"><a href="register">Don't have account? Sign Up Here</a></p>
          </div>
        </div>
			</div>
	</div>

    @include('front_end.layout.footer')
  </body>
</html>
<!-- llllll -->

