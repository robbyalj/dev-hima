<!DOCTYPE html>
<html lang="en">
  <head> 
      @extends('front_end.layout.master')
  </head>

  <body>
  @include('front_end.layout.menu')
   
    <div id="carouselExampleIndicators" class="carousel height-slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
          </ol>
            <div class="carousel-inner height-slide" role="listbox">
              <div class="carousel-item active">
                <img class="d-block img-fluid" src="img/bannerif.png" alt="First slide">
              </div>
                <div class="carousel-item">
                  <img class="d-block img-fluid" src="img/bannerif2.png" alt="Second slide">
                </div>
                  <div class="carousel-item">
                    <img class="d-block img-fluid" src="img/bannerif3.png" alt="Third slide">
                  </div>
            </div>
              <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                 <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                 <span class="sr-only">Previous</span>
              </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
    </div>
    <br><br>

    <!-- JAKET -->

    <section class="content-section-j">

          <section class="content-section-f">
              <div class="container padding-cont2">
                <center>
                <h2 class="section-heading2"><u><p align= "center">JACKETS</p></u></h2>
                <div class="row">
                  
                  <div class="col-md-3">
                    <div class="img-thumbnail">
                      <img class="img-fluid" src="img/cjaket.jpg" alt="">
                      <div class="caption">
                          <h5 class="margin-top">Jacket Programmer</h5>
                          <p>IDR 150.000,-</p>
                          <p><a href="underconstruct" class="btn btn-primary" role="button">Buy</a></p>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="img-thumbnail">
                      <img class="img-fluid" src="img/cjaket.jpg" alt="">
                      <div class="caption">
                          <h5 class="margin-top">Jacket Programmer</h5>
                          <p>IDR 150.000,-</p>
                          <p><a href="underconstruct" class="btn btn-primary" role="button">Buy</a></p>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="img-thumbnail">
                      <img class="img-fluid" src="img/cjaket.jpg" alt="">
                      <div class="caption">
                          <h5 class="margin-top">Jacket Programmer</h5>
                          <p>IDR 150.000,-</p>
                          <p><a href="underconstruct" class="btn btn-primary" role="button">Buy</a></p>
                      </div>
                    </div>
                  </div>

                   <div class="col-md-3">
                    <div class="img-thumbnail">
                      <img class="img-fluid" src="img/cjaket.jpg" alt="">
                      <div class="caption">
                          <h5 class="margin-top">Jacket Programmer</h5>
                          <p>IDR 150.000,-</p>
                          <p><a href="underconstruct" class="btn btn-primary" role="button">Buy</a></p>
                      </div>
                    </div>
                  </div>

                </div><br>
              </div>
          </section>
          <br><br>


<!-- Kaos -->
          <section class="content-section-f">
              <div class="container padding-cont2">
                <center>
                <h2 class="section-heading2"><u><p align= "center">SHIRTS</p></u></h2>
                <div class="row">
                  
                  <div class="col-md-3">
                    <div class="img-thumbnail">
                      <img class="img-fluid" src="img/kaos1.png" alt="">
                      <div class="caption">
                          <h5 class="margin-top">Shirt Programmer</h5>
                          <p>IDR 75.000,-</p>
                          <p><a href="underconstruct" class="btn btn-primary" role="button">Buy</a></p>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="img-thumbnail">
                      <img class="img-fluid" src="img/kaos1.png" alt="">
                      <div class="caption">
                          <h5 class="margin-top">Shirt Programmer</h5>
                          <p>IDR 75.000,-</p>
                          <p><a href="underconstruct" class="btn btn-primary" role="button">Buy</a></p>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="img-thumbnail">
                      <img class="img-fluid" src="img/kaos1.png" alt="">
                      <div class="caption">
                          <h5 class="margin-top">Shirt Programmer</h5>
                          <p>IDR 75.000,-</p>
                          <p><a href="underconstruct" class="btn btn-primary" role="button">Buy</a></p>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="img-thumbnail">
                      <img class="img-fluid" src="img/kaos1.png" alt="">
                      <div class="caption">
                          <h5 class="margin-top">Shirt Programmer</h5>
                          <p>IDR 75.000,-</p>
                          <p><a href="underconstruct" class="btn btn-primary" role="button">Buy</a></p>
                      </div>
                    </div>
                  </div>

                </div><br>
              </div>
          </section>
          <br><br>


<!-- SNACKS -->
          <section class="content-section-f">
              <div class="container padding-cont2">
                <center>
                <h2 class="section-heading2"><u><p align= "center">SNACKS</p></u></h2>
                <div class="row">
                  
                  <div class="col-md-3">
                    <div class="img-thumbnail">
                      <img class="img-fluid" src="img/csnack.jpg" alt="">
                      <div class="caption">
                          <h5 class="margin-top">Snack</h5>
                          <p>IDR 10.000,-</p>
                          <p><a href="underconstruct" class="btn btn-primary" role="button">Buy</a></p>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="img-thumbnail">
                      <img class="img-fluid" src="img/csnack.jpg" alt="">
                      <div class="caption">
                          <h5 class="margin-top">Snack</h5>
                          <p>IDR 10.000,-</p>
                          <p><a href="underconstruct" class="btn btn-primary" role="button">Buy</a></p>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="img-thumbnail">
                      <img class="img-fluid" src="img/csnack.jpg" alt="">
                      <div class="caption">
                          <h5 class="margin-top">Snack</h5>
                          <p>IDR 10.000,-</p>
                          <p><a href="underconstruct" class="btn btn-primary" role="button">Buy</a></p>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-3">
                    <div class="img-thumbnail">
                      <img class="img-fluid" src="img/csnack.jpg" alt="">
                      <div class="caption">
                          <h5 class="margin-top">Snack</h5>
                          <p>IDR 10.000,-</p>
                          <p><a href="underconstruct" class="btn btn-primary" role="button">Buy</a></p>
                      </div>
                    </div>
                  </div>

                </div><br>
              </div>
          </section>
          <br><br>

    </section>

    @include('front_end.layout.footer')
  </body>