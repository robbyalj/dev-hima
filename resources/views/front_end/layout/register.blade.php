<!DOCTYPE html>
<html lang="en">
  <head>
    @extends('front_end.layout.master')
  </head>
  <body> 
  	@include('front_end.layout.menunologin')z

    <div class="container">
		<center><img class="img-fluid" src="img/1.png" style="width: 60px; margin-top: 50px;"></center>
	</div>
	<div class="container login-panel">
			<center><p class="login">Sign Up to HIMATIF Website</p></center>
			<div class="login-form">
				<div class="form-group">
				  <label for="usr">Email:</label>
				  <input type="text" class="form-control" id="usr" placeholder="Email..">
				</div>
				<div class="form-group">
				  <label for="nama">Nama:</label>
				  <input type="password" class="form-control" id="nama" placeholder="Nama..">
				</div>
        <div class="form-group">
          <label for="pwd">Password:</label>
          <input type="password" class="form-control" id="pwd" placeholder="Password..">
        </div>
        <div class="form-group">
          <label for="cnfrm-pwd">Confirm Password:</label>
          <input type="password" class="form-control" id="cnfrm-pwd" placeholder="Confirm Password..">
        </div>
				<button type="button" class="btn btn-success btn-block">Sign Up</button>
        <div class="row">
          <div class="col-sm-6">
            <p class="frgt-pass"><a href="forgotpwd">Forgot password?</a></p>
          </div>
          <div class="col-sm-6">
            <p class="regist"><a href="login">Already Have Account? Sign In Here</a></p>
          </div>
        </div>
			</div>
	</div>

    @include('front_end.layout.footer')
  </body>
</html>
<!-- llllll -->

