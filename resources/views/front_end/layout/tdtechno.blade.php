<!DOCTYPE html>
<html lang="en">
<head>
	<title>HIMATIF Website</title>
	@extends('front_end.layout.master')
</head>
<body>
	@include('front_end.layout.menu')

	<section class="content-section-h">
		<div class="col-md-6">
			<div class="padding-top1">
				<div class="input-group">
			      	<input type="text" class="form-control form-control-minimize" placeholder="Search article...">
			      	<span class="input-group-btn">
			        	<button class="btn btn-warning btn-warning-text">Search</button>
			      	</span>
				</div>
					<br>
				<h2>Cari artikel disini..!</h2><br>
			</div>
		</div>
	</section>
<hr>

<section class="content-section-i">

		<section class="content-section-f">
      <div class="container padding-cont2">
        <div class="row">
             <div class="col-md-8">
                <div class="img-thumbnail">
                  <img class="img-fluid" src="img/bg2.jpg">
                </div>
             </div>

                  <div class="col-md-4"> 
                        <h2 class="section-heading3">Judul Berita</h2>
                          <p class="lead alignj">
                          <font color="black"> Selasa, 20 Januari 2018</font><br>
                          <br>
                            <font color="black"> free photoshop actions! 3D product shots in seconds free Photoshop actions by PSD Covers!
                             3D product shots in seconds free Photoshop actions by PSD Covers!
                              3D product shots in seconds free Photoshop actions by PSD Covers! </font>
                          </p>
                          
                  </div>  
            </div>
          </div>   
    </section>
  <hr width="90%" align="center">

    <section class="content-section-f">
      <div class="container padding-cont2">
        <div class="row">
             <div class="col-md-4">
                <div class="img-thumbnail">
                  <img class="img-fluid" src="img/computer.jpg">
                </div>
             </div>

                  <div class="col-md-8"> 
                        <h2 class="section-heading3">Judul Berita</h2>
                          <p class="lead alignj">
                          <font color="black"> Selasa, 20 Januari 2018</font><br>
                          <br>
                            <font color="black">Turn your 2D designs into high quality, 3D product shots in seconds free Photoshop actions by PSD Covers! 
                              Visit their website to download some of their awesome, free photoshop actions!
                              3D product shots in seconds free Photoshop actions by PSD Covers!
                              3D product shots in seconds free Photoshop actions by PSD Covers! </font>
                          </p>
                          
                  </div>  
            </div>
          </div>   
    </section>
  <hr width="90%" align="center">

    <section class="content-section-f">
      <div class="container padding-cont2">
        <div class="row">
             
                  <div class="col-md-8 ml-auto"> 
                        <h2 class="section-heading5">Judul Berita</h2>
                          <p class="lead alignright">
                           Selasa, 20 Januari 2018<br><br></p>
                           <p class="lead alignj">
                           Turn your 2D designs into high quality, 3D product shots in seconds free Photoshop actions by PSD Covers!
                          Visit their website to download some of their awesome, free photoshop actions!
                          3D product shots in seconds free Photoshop actions by PSD Covers!
                          3D product shots in seconds free Photoshop actions by PSD Covers!

                          </p>
                  </div> 
             <div class="col-md-4">
                   <div class="img-thumbnail">
                  <img class="img-fluid" src="img/computer.jpg">
                </div>
             </div>
 
            </div>
          </div>   
    </section>
    <hr width="90%" align="center">
  
      <section class="content-section-f">
      <div class="container padding-cont2">
        <div class="row">
             <div class="col-md-4">
                <div class="img-thumbnail">
                  <img class="img-fluid" src="img/computer.jpg">
                </div>
             </div>

                  <div class="col-md-8"> 
                        <h2 class="section-heading3">Judul Berita</h2>
                          <p class="lead alignj">
                          <font color="black"> Selasa, 20 Januari 2018</font><br>
                          <br>
                            <font color="black">Turn your 2D designs into high quality, 3D product shots in seconds free Photoshop actions by PSD Covers!
                             Visit their website to download some of their awesome, free photoshop actions!
                             3D product shots in seconds free Photoshop actions by PSD Covers!
                             3D product shots in seconds free Photoshop actions by PSD Covers! </font>
                          </p>
                  </div>  
            </div>
          </div>   
    </section>


   
</section>
	@include('front_end.layout.footer')
</body>
</html>