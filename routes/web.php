<?php

/*
|--------------------------------------------------------------------------

| Web Routesfront_end.master
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//untuk frontend
Route::get('/', function () {
    return view('front_end.home');
});
Route::get('/register', 'RegistrationController@register');
Route::post('/register', 'RegistrationController@postRegister');
Route::post('/login', 'LoginController@postLogin');
Route::get('/login', 'LoginController@login');
Route::post('/logout', 'LoginController@logout');
Route::get('underconstruct', ['uses' => 'UsersController@under']);
Route::get('profile', 'ProfileController@profile');
Route::get('event', 'EventController@event');
Route::get('ifshop', 'IfShopController@ifshop');
Route::get('login', ['uses' => 'LoginUserController@login']);
Route::get('register', ['uses' => 'RegisterController@register']);
Route::get('forgotpwd', ['uses' => 'ForgotpwdController@forgotpwd']);
Route::get('tdtechno', 'TdTechnoController@tdtechno');
Route::get('tdtechno', 'pendaftaranController@index');



//untuk backend
Route::get('hsttb18', function () {
    return view('back_end.index_adm.tampil_page');
});
//calon agota (robby)
Route::get('hsttb18/panggota', 'back_end\pendaftaran\pendaftaranController@index');
Route::get('hsttb18/panggota/vdtl/{id}', 'back_end\pendaftaran\pendaftaranController@viewDetail');
Route::get('hsttb18/panggota/insrt', 'back_end\pendaftaran\pendaftaranController@viewInsert');
Route::post('file/upload', 'back_end\pendaftaran\pendaftaranController@upload')->name('file.upload');

//slide (sandika)
Route::get('hsttb18/slide','back_end\slide\SlideController@index');
//Route::get('Home/ViewDetail/{$id}','back_end\slide\SlideController@show');

//slide (afandi)
Route::get('hsttb18/logo','back_end\slide\SlideController@index');
//Route::get('Home/ViewDetail/{$id}','back_end\slide\SlideController@show');
